#intalar image_slicer con pimp
import image_slicer
#PIL ya viene instalado
from PIL import ImageColor, Image

#Se abre la imagen con el modulo Image
imagen =  Image.open('colores.png')

#La imagen se divide con slice de izquierda a derecha y de arriba a abajo
#Y se guarda en un arreglo unidimensional
tiles = image_slicer.slice('colores.png',16)

#Para acceder a los pixeles de las imagenes se usa la funcion load() del objeto Image
pixelGrande = imagen.load()

#Lo mismo con el arreglo de imagenes divididas
pixel = tiles[1].image.load()

#Size te da el tamaño de la imagen
print(imagen.size)
#La informacion por defecto de los pixeles es un vector RGB 
print(pixelGrande[600,600])