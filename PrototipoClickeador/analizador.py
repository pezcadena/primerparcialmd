import cv2
import numpy as np
from numpy import mean, std
from scipy import stats
from tkinter import filedialog, Tk
from PIL import Image
import image_slicer
from skimage import color
from skimage.util import img_as_ubyte
from skimage.filters.rank import entropy
from skimage.morphology import disk

def getData(imagePil):
    
    """ file_path = filedialog.askopenfilename()
    chunks = image_slicer.slice(file_path,49,save=False)
    #leemos la imagen, por defecto la lee en BGR
    imagePil = chunks[0].image.convert("RGB")
    #print("[B G R]")
    #print(image[0,0]) """
    image = np.array(imagePil)

    #convertimos la imagen de RGB a HSV
    imagehsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    #convertimos la imagen de RGB a LAB - solo utilizamos el parametro L
    imagelab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

    #obtenemos el alto, ancho y el numero de canales con que trabaja la imagen
    alto, ancho, canales = image.shape
    #print("altura: ",alto, "anchura:",ancho)
    cantidadPixeles = alto*ancho

    #declaramos las listas donde se almacenaran los datos de la imagen leida
    XH = []
    XS = []
    XV = []
    XR = []
    XG = []
    XB = []
    XL = []
    i=0
    j=0

    #Obtener datos de la imagen y almacenarlos en listas de datos
    while i < (alto):
        while j < (ancho):
            #recoleccion RGB
            b, g, r = image[i,j]
            XR.append(r)
            XG.append(g)
            XB.append(b)
            
            #recoleccion HSV
            h, s, v = imagehsv[i,j]
            XH.append(h*2) #obtenemos el valor de 360°
            XS.append(s*100/255) #convertimos a porcentaje de 0 - 100
            XV.append(v*100/255) #convertimos a porcentaje de 0 - 100

            #recoleccion de Lab
            l, a, b = imagelab[i,j]
            XL.append(l*100/255) #convertimos a porcentaje de 0 - 100

            j += 1
        i += 1
        j = 0

    #calculo de promedios
    promR = int(round(mean(XR)))
    promG = int(round(mean(XG)))
    promB = int(round(mean(XB)))
    promH = int(round(mean(XH)))
    promS = int(round(mean(XS))) 
    promV = int(round(mean(XV))) 
    promL = int(round(mean(XL)))

    #calculo de desviación estandar
    desvR = int(round(std(XR),2))
    desvG = int(round(std(XG),2))
    desvB = int(round(std(XB),2))
    desvH = int(round(std(XH),2))
    desvS = int(round(std(XS),2))
    desvV = int(round(std(XV),2))
    desvL = int(round(std(XL),2))

    #calculo de moda
    modaR = stats.mode(XR)
    cantidadR = modaR[1][0]
    modaR = modaR[0][0]
    modaG = stats.mode(XG)
    cantidadG = modaG[1][0]
    modaG = modaG[0][0]
    modaB = stats.mode(XB)
    cantidadB = modaB[1][0]
    modaB = modaB[0][0]

    modaH = stats.mode(XH)
    cantidadH = modaH[1][0]
    modaH = round(modaH[0][0])
    modaS = stats.mode(XS)
    cantidadS = modaS[1][0]
    modaS = int(round(modaS[0][0]))
    modaV = stats.mode(XV)
    cantidadV = modaV[1][0]
    modaV = int(round(modaV[0][0]))
    
    img = img_as_ubyte(color.rgb2gray(image))

    entr_img = entropy(img, disk(10))

    promE = 0
    desvE = 0
    promE = int(round(mean(entr_img)))
    desvE = int(round(std(entr_img),2))

    listaValores = []
    listaValores.append(promR)
    listaValores.append(promG)
    listaValores.append(promB)
    listaValores.append(promH)
    listaValores.append(promS)
    listaValores.append(promV)
    listaValores.append(promL)
    listaValores.append(desvR)
    listaValores.append(desvG)
    listaValores.append(desvB)
    listaValores.append(desvH)
    listaValores.append(desvS)
    listaValores.append(desvV)
    listaValores.append(desvL)
    listaValores.append(modaR)
    listaValores.append(modaG)
    listaValores.append(modaB)
    listaValores.append(modaH)
    listaValores.append(modaS)
    listaValores.append(modaV)
    listaValores.append(promE)
    listaValores.append(desvE)
    listaValores.append(cantidadPixeles)
    print(listaValores)

    return listaValores

""" print("Cantida de pixeles: ",cantidadPixeles)
print("Promedio luminosidad",promL,"%")
print("Desviacion estandar Luminosidad",desvL)
print("\n")
print("Valores promedio -R:",promR," -G:",promG," -B:",promB)
print("Desviacion estandar -R:",desvR," -G:",desvG," -B:",desvB)
print("Valores de moda en -R:",modaR,"con",cantidadR,"elementos" )
print("Valores de moda en -G:",modaG,"con",cantidadG,"elementos" )
print("Valores de moda en -B:",modaB,"con",cantidadB,"elementos" )
print("\n")
print("Valores promedio H:",promH," S:",promS," V:",promV)
print("Desviacion estandar H:",desvH," S:",desvS," V:",desvV)
print("Valores de moda en -H:",modaH,"con",cantidadH,"elementos" )
print("Valores de moda en -S:",modaS,"con",cantidadS,"elementos" )
print("Valores de moda en -V:",modaV,"con",cantidadV,"elementos" ) """
