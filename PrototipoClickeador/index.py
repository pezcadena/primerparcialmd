# ─── SECCION DE IMPORTACION ─────────────────────────────────────────────────────
from PIL import Image
import image_slicer
#la parte grafica se hace con pygame y ya no con pyglet porque no rifa jajajajaj
import pygame, sys, os
from pygame.locals import *
from pygame import mouse, image, transform, draw
#se usa tkinter para preguntar sobre la imagen
from tkinter import filedialog, Tk
from analizador import getData
#para recorrer los archivos del directorio
from os import listdir, getcwd
from os.path import isfile, join


# ─── SECCION DE OBJETOS ─────────────────────────────────────────────────────────

#Creacion de un objeto fuego
class fuego:
    def __init__(self, pos, numtile):
        super().__init__()
        dirfuego = resource_path("fuego.png")
        self.fuego =  pygame.image.load(dirfuego)
        self.fuego = self.fuego.convert()
        color = self.fuego.get_at((0,0))
        self.fuego.set_colorkey(color, RLEACCEL)
        self.fuego = pygame.transform.scale(self.fuego, (50, 50))
        self.posfuego = pos
        self.numtile=numtile
        self.estado=True

#Creacion de un objeto humo
class humo:
    def __init__(self, pos, numtile):
        super().__init__()
        dirhumo =  resource_path("humo.jpg")
        self.humo =  pygame.image.load(dirhumo)
        self.humo = self.humo.convert()
        color = self.humo.get_at((0,0))
        self.humo.set_colorkey(color, RLEACCEL)
        self.humo = pygame.transform.scale(self.humo, (50, 50))
        self.poshumo = pos
        self.numtile=numtile
        self.estado=True

class listo:
    def __init__(self):
        super().__init__()
        dirlisto = resource_path("listo.png")
        self.listo =  pygame.image.load(dirlisto)
        self.listo = self.listo.convert()
        color = self.listo.get_at((0,0))
        self.listo.set_colorkey(color, RLEACCEL)
        self.listo = pygame.transform.scale(self.listo, (50, 50))
        self.estado=True

class espera:
    def __init__(self):
        super().__init__()
        direspera = resource_path("espera.png")
        self.espera =  pygame.image.load(direspera)
        self.espera = self.espera.convert()
        color = self.espera.get_at((0,0))
        self.espera.set_colorkey(color, RLEACCEL)
        self.espera = pygame.transform.scale(self.espera, (50, 50))
        self.estado=False


# ─── SECCION DE FUNCIONES ───────────────────────────────────────────────────────

def resource_path(relative_path):
    try:
    # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

#funcion que pregunta por la imagen a cargar
def cargarImagen():
    #Se llama a tkinter para preguntar que imagen se cargará
    root = Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename()
    return file_path

#función que convierte los pedazos en formato de pygame
def conversorImagen(tile):
    raw_str = tile.image.tobytes("raw", 'RGB')
    resul = pygame.image.fromstring(raw_str, tile.image.size, 'RGB')
    return pygame.transform.scale(resul,(100,50))#cambia el tamaño de la imagen a 100x50

#funcion que detecta la matriz y devuelve la pos del objeto a dibujar
def posenmatriz(pos):
    lst = list(pos)
    x=lst[0]
    y=lst[1]
    num=0
    numFinal=0
    xfinal=0
    yfinal=0
    esta=True

    for i in range(0,350,50):
        for j in range(0,700,100):
            num+=1
            if x > j and x < (j+100) and y > i and y < (i+50):
                numFinal=num
                xfinal=j
                yfinal=i
                esta=True
                break
            else:
                esta=False
        if esta:
            break
    
    posfinal=(xfinal,yfinal)
    return numFinal, posfinal, esta

#funcion de escritura del archivo arff
def archArff(listfuegos,listhumos,chunks,numFotos):
    band = True
    if not isfile("datos.arff"):
        arff = open("datos.arff",'w')
        arff.write("@relation 'Incendios Forestales' \n\n")
        arff.write("@attribute PromedioR NUMERIC\n@attribute PromedioG NUMERIC\n@attribute PromedioB NUMERIC\n@attribute PromedioH NUMERIC\n@attribute PromedioS NUMERIC\n@attribute PromedioV NUMERIC\n@attribute PromedioLuz NUMERIC\n@attribute DesviacionR NUMERIC\n@attribute DesviacionG NUMERIC\n@attribute DesviacionB NUMERIC\n@attribute DesviacionH NUMERIC\n@attribute DesviacionS NUMERIC\n@attribute DesviacionV NUMERIC\n@attribute DesviacionLuz NUMERIC\n@attribute ModaR NUMERIC\n@attribute ModaG NUMERIC\n@attribute ModaB NUMERIC\n@attribute ModaH NUMERIC\n@attribute ModaS NUMERIC\n@attribute ModaV NUMERIC\n")
        arff.write("@attribute PromedioEntropia NUMERIC\n@attribute DesviacionEntropia NUMERIC\n")
        arff.write("@attribute CantidadPixeles NUMERIC\n")
        arff.write("@attribute CLASIFICADOR {bosque, fuego, humo}\n")
        arff.write("\n")
        arff.write("@data\n")
        for i in range(1,50):
            
            listainfo = getData(chunks[i-1].image.convert("RGB"))

            for k in listainfo:
                arff.write(str(k)+",")

            band = True

            for f in listfuegos:
                if f.numtile == i:
                    if f.estado:
                        arff.write("fuego\n")
                        band = False

            for h in listhumos:
                if h.numtile == i:
                    if h.estado:
                        arff.write("humo\n")
                        band = False

            if band:
                arff.write("bosque\n")
        arff.close()
    else:
        arff = open("datos.arff",'a')
        # ─── ATRIBUTO CLASIFICADOR ──────────────────────────────────────────────────────
        arff.write("%"+str(numFotos)+"\n")
        for i in range(1,50):
            
            listainfo = getData(chunks[i-1].image.convert("RGB"))

            for k in listainfo:
                arff.write(str(k)+",")

            band = True

            for f in listfuegos:
                if f.numtile == i:
                    if f.estado:
                        arff.write("fuego\n")
                        band = False

            for h in listhumos:
                if h.numtile == i:
                    if h.estado:
                        arff.write("humo\n")
                        band = False

            if band:
                arff.write("bosque\n")
        arff.close()
        

# ─── SECCION DE VARIABLES ───────────────────────────────────────────────────────



#Se sacan las constantes de altura y anchura de la imagen
WIDTH = 700
HEIGHT = 400

#Se divide la imagen en los pedazos y se guarda en una colección llamada chunks

#Se crean mas constantes
ima = {} #colección de los pedazos
#altura y anchura de los pedazos
tilewidth = 100
tileheight = 50

#Arreglo de fuegos y humos
fuegos=[]
humos=[]




# ─── MAIN ───────────────────────────────────────────────────────────────────────

def main():
    # ─── VARIABLES DEL MAIN ─────────────────────────────────────────────────────────
    
    root = Tk()
    root.withdraw()
    folder_selected = filedialog.askdirectory()
    print(folder_selected)

    onlyfiles = [f for f in listdir(folder_selected) if isfile(join(folder_selected, f))]

    for fichier in onlyfiles[:]: # filelist[:] makes a copy of filelist.
        if (not(fichier.endswith(".png"))) and (not(fichier.endswith(".jpeg"))) and (not(fichier.endswith(".jpg"))):
            onlyfiles.remove(fichier)

    onlyfiles =  sorted(onlyfiles)

    tamfolder = len(onlyfiles)
    print(tamfolder)
    print(onlyfiles)

    rutaImagen=folder_selected+"/"+onlyfiles[0]

    imagen = Image.open(rutaImagen)
    chunks = image_slicer.slice(rutaImagen,49,save=False)
    i=0
    for pedaso in chunks:
        ima[i] = conversorImagen(pedaso)
        i+=1
    
    #se crea las dimensiones de la ventana y su nombre
    screen = pygame.display.set_mode((WIDTH+9, HEIGHT+9))
    pygame.display.set_caption("Clickeador de imágenes")
    
    iconolisto = listo()
    iconolisto.estado=False
    iconoespera = espera()
    iconoespera.estado=False

    numfotos=1
    
    #ciclo que convierte todos los pedazos en el formato de pygame (posiblemente se haga función void)
    

    # ─── CICLO INFINITO DEL PROGRAMA ────────────────────────────────────────────────

    while True:
        #Se rellena la pantalla de blanco
        screen.fill((255,255,255))
        #Botones
        font = pygame.font.Font(None, 11)
        textAnalizar = font.render('ANALIZAR', True, [255,255,255]) 
        textSalir = font.render('SALIR '+str(numfotos), True, [255,255,255]) 
        textSiguiente = font.render('SIGUIENTE', True, [255,255,255]) 
        textAnterior = font.render('ANTERIOR', True, [255,255,255]) 
        buttonSalir = pygame.Rect(620, 371, 70, 30)
        pygame.draw.rect(screen, [0, 0, 0], buttonSalir)  # draw button
        buttonAnalizar = pygame.Rect(22, 371, 70, 30)
        pygame.draw.rect(screen, [255, 0, 0], buttonAnalizar)  # draw button
        buttonSiguiente = pygame.Rect(181, 371, 70, 30)
        pygame.draw.rect(screen, [0, 255, 0], buttonSiguiente)
        buttonAnterior = pygame.Rect(110, 371, 70, 30)
        pygame.draw.rect(screen, [0, 0, 255], buttonAnterior)
        screen.blit(textAnalizar, (29,382)) 
        screen.blit(textAnterior, (117,382)) 
        screen.blit(textSalir, (632,382)) 
        screen.blit(textSiguiente,(187,382))

        
        
        # ─── EVENTOS ─────────────────────────────────────────────────────

        for eventos in pygame.event.get():
            
            m1, m2, m3 = pygame.mouse.get_pressed()
            
            # ─── CLICK IZQUIERDO ─────────────────────────────────────────────

            if m1 == 1:
                print(pygame.mouse.get_pos())
                lst1 = pygame.mouse.get_pos()
                numtile, pos1, esta1 = posenmatriz(lst1)

                if esta1:
                    if(len(fuegos)==0):
                        fuegos.append(fuego(pos1,numtile))
                    else:
                        bandera = True
                        for f in fuegos:
                            if f.numtile == numtile:
                                bandera = False
                                if f.estado:
                                    f.estado = False
                                else:
                                    f.estado = True
                        if bandera:
                            fuegos.append(fuego(pos1,numtile))
               
            # ─── CLICK DERECHO ───────────────────────────────────────────────

            if m3 == 1:
                print(pygame.mouse.get_pos())
                lst2 = pygame.mouse.get_pos()
                numtile2, pos2, esta2 = posenmatriz(lst2)

                if esta2:
                    if(len(humos)==0):
                        humos.append(humo(pos2,numtile2))
                    else:
                        bandera = True
                        for f in humos:
                            if f.numtile == numtile2:
                                bandera = False
                                if f.estado:
                                    f.estado = False
                                else:
                                    f.estado = True
                        if bandera:
                            humos.append(humo(pos2,numtile2))
                
            #Evento que mata la aplicacion
            if eventos.type == QUIT:
                sys.exit(0)

            # ─── EVENTO DEL BOTON ────────────────────────────────────────────

            if eventos.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = eventos.pos  # gets mouse position
                # checks if mouse position is over the button
                
                # BOTON SALIR
                if buttonSalir.collidepoint(mouse_pos):
                    # prints current location of mouse
                    print('buttonSalir was pressed at {0}'.format(mouse_pos))
                    sys.exit(0)
                
                # BOTON ANALIZAR
                if buttonAnalizar.collidepoint(mouse_pos):
                    # prints current location of mouse
                    screen.blit(iconoespera.espera,(350,200))
                    pygame.display.update()
                    pygame.display.flip() 
                    print('buttonAnalizar was pressed at {0}'.format(mouse_pos))
                    archArff(fuegos,humos,chunks,numfotos)
                    iconolisto.estado=True
                    print(numfotos)
                #BOTON SIGUIENTE
                if buttonSiguiente.collidepoint(mouse_pos):
                    numfotos+=1
                    if numfotos > tamfolder:
                        sys.exit(0)
                    rutaImagen=folder_selected+"/"+onlyfiles[numfotos]
                    print(rutaImagen)
                    imagen = Image.open(rutaImagen)
                    chunks = image_slicer.slice(rutaImagen,49,save=False)
                    i=0
                    for pedaso in chunks:
                        ima[i] = conversorImagen(pedaso)
                        i+=1
                    iconolisto.estado=False
                    fuegos.clear()
                    humos.clear()
                #BOTON ANTERIOR
                if buttonAnterior.collidepoint(mouse_pos):
                    numfotos-=1
                    if numfotos == 0:
                        sys.exit(0)
                    rutaImagen=folder_selected+"/"+onlyfiles[numfotos]
                    imagen = Image.open(rutaImagen)
                    chunks = image_slicer.slice(rutaImagen,49,save=False)
                    i=0
                    for pedaso in chunks:
                        ima[i] = conversorImagen(pedaso)
                        i+=1
                    iconolisto.estado=False
                    fuegos.clear()
                    humos.clear()

                
        # ─── IMPRESION EN PANTALLA ────────────────────────────────────────
        
        #Constantes para la impresión de los pedazos
        x = -tilewidth-1
        y = 0
        #Ciclo que se encarga de imprimir los pedazos
        for im in ima:
            x += tilewidth+1
            if (x>=WIDTH-5):
                x = 0
                y += tileheight+1
                screen.blit(ima[im], (x, y))
            else:
                screen.blit(ima[im], (x, y))

        #Ciclos que dibujan los iconos
        for f in fuegos:
            if f.estado:
                screen.blit(f.fuego,f.posfuego)
        for h in humos:
            if h.estado:
                screen.blit(h.humo,h.poshumo)
        if iconolisto.estado:
            screen.blit(iconolisto.listo, (315,350))

        
        
        
        #Función que refresca la pantalla  
        pygame.display.update()
        pygame.display.flip()    
        
    return 0

if __name__ == '__main__':
    pygame.init()
    main()